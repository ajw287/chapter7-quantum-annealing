#!/usr/bin/env python3
#
#                                                 Copyright 2019 Andrew Whyte
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
#  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

try:
    import sys
    import os
    import time
    import threading
    import dimod as dmd
    import numpy as np
    import matplotlib.pyplot as plt
    import dwave_networkx as dnx
    import dwave as dwv
    import networkx as nx
    import minorminer
    from PIL import Image
    from itertools import chain
    from itertools import cycle
except ImportError as error:
	#print("\n\ntry the command: >conda install -c conda-forge keras\n\n")
    install_module_msg = "It looks like a required module isn't installed.\n\n" + \
                         "The required dependencies are:  sys, os, time, threading,\n"+\
                         "                                dimod, numpy, matplotlib, dwave_networkx,\n" + \
                         "                                networkx, minorminer, itertools and PIL\n\n"+ \
                         "Probably the command:\n>pip3 install <module_name>\n will do the trick.\n\n"
    print(exception.__class__.__name__+":"+error.message+"\n\n")
    raise ImportError(install_module_msg)
else:
	print("All Dependencies imported")

cursor_global = '-'
class SpinningCursor:
    '''
    Class animates the cursor to let you know the code is doing something

    A spinning cursor class, from:
    https://stackoverflow.com/questions/4995733/how-to-create-a-spinning-command-line-cursor
    by victor Moyseenko
    '''

    busy = False
    delay = 0.5
    @staticmethod
    def spinning_cursor():
        while 1:
            for cursor in '|/-\\': yield cursor


    def __init__(self, delay=None):
        self.spinner_generator = self.spinning_cursor()
        if delay and float(delay): self.delay = delay

    def spinner_task(self):
        while self.busy:
            sys.stdout.write(next(self.spinner_generator))
            sys.stdout.flush()
            time.sleep(self.delay)
            sys.stdout.write('\b')
            sys.stdout.flush()

    def __enter__(self):
        self.busy = True
        threading.Thread(target=self.spinner_task).start()

    def __exit__(self, exception, value, tb):
        self.busy = False
        time.sleep(self.delay)
        if exception is not None:
            return False


def get_J_and_h(r_index, n_batches):
    '''
    A function returns  generates the dictionaries, J & h.

    These varaibles are the basis for the Ising model of the core, setting the weights is the
    most important part of defining the problem.
    Inputs:
    ------
    r_index: int
        The region array to use
    n_qubits: int
        how many cubits are in the model (225 for two batch, 450 for three batch)

    Returns:
    ------
        dictionary (,)
        The 'h' dictionary describes the transverse field of the Ising model
        dictionary (,)
        the 'J' dictionary describes the exchange constant (which describes how qubits are connected)

    '''
    # the list of grid positions that aren't in the core (don't assign a qubit here.)
    excluded_list  = [   0,   1,   2,   3,  11,  12,  13,  14,  15,  16,  28,  29,  30,  44,  45,  59,
                       165, 179, 180, 194, 195, 196, 208, 209, 210, 211, 212, 213, 221, 222, 223, 224]
    not_blanket_1 = list(chain(  list(range( 50, 54+1)), list(range( 64, 70+1)), list(range( 78, 86+1)),
                                 list(range( 93,101+1)), list(range(108,116+1)), list(range(123,131+1)),
                                 list(range(138,146+1)), list(range(154,160+1)), list(range(170,174+1)),
                                 list(range(275,279+1)), list(range(289,295+1)), list(range(303,311+1)),
                                 list(range(318,326+1)), list(range(333,341+1)), list(range(348,356+1)),
                                 list(range(363,371+1)), list(range(379,385+1)), list(range(395,399+1)), ))

    # BEAVRS large inner section
    not_blanket_3 = list(chain( list(range( 20, 24+1)), list(range( 33, 41+1)), list(range( 47, 57+1)),
                                list(range( 62, 72+1)), list(range( 76, 88+1)), list(range( 91,103+1)),
                                list(range(106,118+1)), list(range(121,133+1)), list(range(136,148+1)),
                                list(range(152,162+1)), list(range(167,177+1)), list(range(183,191+1)),
                                list(range(200,204+1)),
                                list(range(245,249+1)), list(range(258,266+1)), list(range(272,282+1)),
                                list(range(287,297+1)), list(range(301,313+1)), list(range(316,328+1)),
                                list(range(331,343+1)), list(range(346,358+1)), list(range(361,373+1)),
                                list(range(377,387+1)), list(range(392,402+1)), list(range(408,416+1)),
                                list(range(425,429+1)) ))
    h={}
    J={}
    if r_index == 1:
        not_blanket = not_blanket_1
    elif r_index == 3:
        not_blanket = not_blanket_3
    else:
        print("Region index out of range")
        exit()
    if n_batches == 2:
        n_qubits = 225
    elif n_batches==3:
        n_qubits = 450
    else:
        n_qubits = 0
        print("Error: batch number incorrect")
        exit()
    for i in range(0,int(225)):
        if i not in excluded_list:
                if i in not_blanket:
                    if n_batches == 3:# then 85, else 50
                        h[i] = 85.0
                    else:
                        h[i] = 50.0
                    #print(str(i)+": 85.000,")
                else:
                    h[i]=-51.0
                    #print(str(i)+":-50.000,")
    if n_batches == 3:
        for i in range(225, n_qubits):
            if i not in excluded_list:
                    if i in not_blanket:
                        h[i] = 2.0
                    else:
                        h[i] = -6.0
    for i in range(0, n_qubits):
        if i not in excluded_list:
            if i < 225:
                # always connect adjacents for 2 batches,
                # in 3 batches, use the transverse field
                # to define the first batch
                if i not in not_blanket or n_batches <3:
                    i_plus = i+1
                    if i_plus not in excluded_list:
                        J[(i, i_plus)] = 10.0
                    if i < 210: # no connection below second last row
                        i_b1 = i+14
                        if i_b1 not in excluded_list:
                            J[(i, i_b1)] = 7.071
                        i_b2 = i+15
                        if i_b2 not in excluded_list:
                            J[(i, i_b2)] = 10.0
                        i_b3 = i+16
                        if i_b3 not in excluded_list:
                            J[(i, i_b3)] = 7.071
            if i > 225:
                if i in not_blanket:
                    i_b0 = i+1
                    if i_b0 not in excluded_list:
                        J[(i, i_b0)] = 1.0
                    i_b1 = i+14
                    if i_b1 not in excluded_list:
                        J[(i, i_b1)] = 0.125
                    i_b2 = i+15
                    if i_b2 not in excluded_list:
                        J[(i, i_b2)] = 1.0
                    i_b3 = i+16
                    if i_b3 not in excluded_list:
                        J[(i, i_b3)] = 0.125
                    i_b4 = i-14
                    i_b5 = i-15
                    i_b6 = i-16
                    i_b7 = i-1

                    if  i_b0 not in not_blanket:
                        if i_b0 not in excluded_list:
                            J[(i, i_b0-225)] = 1.0
                    if  i_b1 not in not_blanket:
                        if i_b1 not in excluded_list:
                            J[(i, i_b1-225)] = 0.7071
                    if  i_b2 not in not_blanket:
                        if i_b2 not in excluded_list:
                            J[(i, i_b2-225)] = 1.0
                    if  i_b3 not in not_blanket:
                        if i_b3 not in excluded_list:
                            J[(i, i_b3-225)] = 0.7071
                    if  i_b4 not in not_blanket:
                        if i_b4 not in excluded_list:
                            J[(i, i_b4-225)] = 0.7071
                    if  i_b5 not in not_blanket:
                        if i_b5 not in excluded_list:
                            J[(i, i_b5-225)] = 1.0
                    if  i_b6 not in not_blanket:
                        if i_b6 not in excluded_list:
                            J[(i, i_b6-225)] = 0.7071
                    if  i_b7 not in not_blanket:
                        if i_b7 not in excluded_list:
                            J[(i, i_b7-225)] = 1.0
    return J,h

def main():
    '''
    Define a PWR core as an Ising model then optimise with simulated quantum annealer

    This script finds the lowest energy arrangement of assemblies in a generic PWR core
    based on a set of rules similar to Galperin

    '''
    # Some global variables
    height = 15
    width = 15
    num_batches = 0

    while 2 != num_batches and 3 !=num_batches:
        batch_text = input("\nModel a 2 or 3 batch fuel loading pattern?  (input '2' or '3'):")
        if batch_text == '2':
            num_batches = 2
        elif batch_text == '3':
            num_batches = 3
        else:
            print("Unknown input input must be a '2' or a '3'.")

    # Create the Ising model in terms of J & h
    got_region_size  = False
    region_index = 0
    while  not got_region_size:
        region_text = input("\nChoose the thickness of the outer radial section ('1' = big or '2' = small ):")
        if region_text == '1':
            region_index = 1
            got_region_size = True
        elif region_text == '2':
            region_index = 3
            got_region_size = True
        else:
            print("Unknown input input, type a '1' or a '2'")
    print("\nGenerating the Ising model ...", end=' ')
    J,h = get_J_and_h(region_index, num_batches)
    print("complete")

    print("Creating Binary Quadratic model ...", end=' ')
    model = dmd.BinaryQuadraticModel(h, J, 0.0, dmd.SPIN)
    print("complete")

    print("Carrying out Simulated Quantum Annealing...")
    print("This stage takes a few minutes ... ", end=' ')

    sampler = dmd.SimulatedAnnealingSampler()
    print("(at least long enough for a coffee!)", end=' ')
    with SpinningCursor():
        response = sampler.sample(model, num_reads=1000)
    print(" complete")

    solutions = [solution for solution in response.data()]
    energies =[solution.energy for solution in response.data()]
    best = solutions[energies.index(np.min(energies))]

    # make an image from a core
    height = 15
    width = 15
    data = np.zeros((height, width, 3), dtype=np.uint8)
    for x in range(width):
        red = [245,140,132]
        yellow = [255,240,137]
        blue = [112,153,251]
        for y in range(height):
            val = best.sample.get(y*15 + x, 0)
            if val  == 1:
                data[y, x] = blue
            elif val == -1:
                val_2 = best.sample.get(225+y*15 + x, 0)
                if val_2 == 1:
                    data[y, x] = red
                elif val == -1:
                    data[y, x] = yellow
            elif val ==0:
                data[y, x] = [0, 0, 0]
            else:
                exit()
    img = Image.fromarray(data, 'RGB')
    # resize the image to make it more visible
    output_image = img.resize((1600,1600),Image.NEAREST)

    i=0
    filename = 'core_loading_pattern_{:03}.png'.format(i)
    while os.path.exists(filename):
        i+=1
        filename = 'core_loading_pattern_{:03}.png'.format(i)

    output_image.save(filename, format='PNG')

    print("the output image is saved to '"+filename+"'\n\n")

    do_minor_embedding = 0
    while 1 != do_minor_embedding:
        embed = input("\nEmbed this Ising model into a chimera graph? ('y'-contine 'n'-exit):")
        embed.lower()
        if embed == 'y' or embed == 'yes':
            do_minor_embedding =1
        elif embed == 'n' or embed == 'no':
            print("\nThank you for using the Quantum Annealing Core loading pattern generator")
            exit()
        else:
            print("Unknown input input must be a 'y' or a 'n'.")
    size_of_graph = 0
    while 0 == size_of_graph:
        chimera_size = input("\nDefine the size of the chimera graph? (or enter to use the default, 16x16): ")
        if chimera_size.isdigit():
            size_of_graph = int(chimera_size)
            if size_of_graph <0 or size_of_graph > 50:
                print("Graph size out of range...")
                size_of_graph = 0
            print("\nUsing length: "+str(size_of_graph)+"x"+str(size_of_graph)+"")
        elif chimera_size is None:
            print("\nUsing default length 16x16")
            size_of_graph
        else:
            print("Unknown input input must be an integer between 0 and 50")

    print("Re creating model for dwave...", end=' ')

    G=nx.Graph()
    connectivity_structure = dnx.chimera_graph(size_of_graph, size_of_graph)
    G.add_nodes_from(h.keys())
    G.add_edges_from(J.keys())
    print("complete")

    print("Trying to fit model into 16x16 chimera graph...", end=' ')
    with SpinningCursor():
        embedded_graph = minorminer.find_embedding(G.edges(), connectivity_structure.edges())
    print("complete")

    dnx.draw_chimera_embedding(connectivity_structure, embedded_graph)
    max_chain_length = 0
    for _, chain in embedded_graph.items():
        if len(chain) > max_chain_length:
            max_chain_length = len(chain)
    print("max chain lenght: "+ str(max_chain_length))
    plt.show()
    exit()

if __name__ == "__main__":
    main()
