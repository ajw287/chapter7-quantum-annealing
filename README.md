# README #

This is the code that is associated with Chapter 7 of my thesis.  Application of Quantum Annealing to PWR fuel loading pattern optimisation.

* Version 0.1

### How do I get set up? ###

* Summary of set up
This code requires an installation of python3.

* Dependencies
The code should run reasonably easily.  The following dependencies will probably need to be installed
 >  pip3 install dimod
 >  pip3 install dwave_networkx
 >  pip install dwave-ocean-sdk
then run the code... for example:
 >  python3 whyte_batch_simulator.py 

* Deployment instructions
Maintain the directory structure shown here.

### Contribution guidelines ###
No contributions are expected, though please contact the author if you are interested in collaborations.

### Who do I talk to? ###

* Repo owner : ajw287@cam.ac.uk
