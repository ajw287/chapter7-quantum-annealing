#!/usr/bin/env bash
echo "This script generates a two batch LP with large blanket"
echo "The script 'whyte_fuel_loading' is called with parameters '2', '1':"
printf '2\n1\n\n' | python3 ./code/whyte_fuel_loading.py
echo "Output LP is generated in the execution directory as a PNG file: "
